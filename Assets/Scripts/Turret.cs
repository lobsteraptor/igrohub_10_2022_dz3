using UnityEngine;

public class Turret : MonoBehaviour
{
    private float _timerFire = 0f;
    [SerializeField] private GameObject _bulletPrefab;
    [SerializeField] private GameObject _bulletSpawner;
    [SerializeField] private float _fireDelay = 30f;
   
    private void Update()
    {
        Fire();
    }

    private void Fire()
    {
        _timerFire -= Time.deltaTime;

        if (_timerFire <= 0)
        {
            Instantiate(_bulletPrefab, _bulletSpawner.transform.position, _bulletSpawner.transform.rotation);
            _timerFire = _fireDelay;
        }
    }

}
