using UnityEngine;
namespace Game
{
public class Door : MonoBehaviour
{
    private float _timer = 0f;
    private bool _lockActive;
    [SerializeField] private float _lockDelay;
    [SerializeField] private GameObject _door;

    private void LockAtTimerTick()
    {
        _timer -= Time.deltaTime;

        if (_timer <= 0)
        {
            _timer = _lockDelay;
            if (_lockActive == true)
            {
                _door.SetActive(false);
                _lockActive = false;
            }
            else {
                _door.SetActive(true);
                _lockActive = true;
            }
               
        }
    }

    private void Update()
    {
        LockAtTimerTick();
    }

}
}
