using UnityEngine;
namespace Game
{
    public class DoorKey : MonoBehaviour
{
    [SerializeField] private GameObject _door;
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            Destroy(_door);
            Destroy(gameObject);
        }
    }
}
}
