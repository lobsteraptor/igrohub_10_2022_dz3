using UnityEngine;
namespace Game
{
public class Portal : MonoBehaviour
{
    [SerializeField] private GameObject _portalOut;
    private void OnTriggerEnter(Collider other)
    {
            if (other.TryGetComponent(out Player player)) {
                player.transform.position = _portalOut.transform.position;
            }
    }
}
}
