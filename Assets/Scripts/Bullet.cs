using UnityEngine;
namespace Game
{
[RequireComponent(typeof(Rigidbody))]

    public class Bullet : MonoBehaviour
{
    [SerializeField] private float _bulletSpeed = 10f;
    [SerializeField] private Rigidbody _rigidbody;
    private void Update()
    {
        _rigidbody.velocity = transform.forward * _bulletSpeed;
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            player.Kill();
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

}
}
